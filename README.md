# Tank Server

Instalacja:

```bash
pip install pipenv
pipenv install
```

Uruchamianie:

```bash
python3 run.py
```

Uruchamianie serwera testowego:

```bash
python3 run.py --debug server --mock
```