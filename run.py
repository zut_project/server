#!/usr/bin/env python3
import time
import logging
from argparse import ArgumentParser

from SimpleWebSocketServer import SimpleWebSocketServer

from tank.robot import MockRobot
from tank.server import ServerDelegate
from tank.turret import MockTankTurret
from tank.util.pca9685 import PCA9685


def pulse(args):
    pwm = PCA9685(args.address)
    pwm.set_pwm_freq(args.freq)
    pwm.set_servo_pulse(args.channel, args.value)
    time.sleep(0.02)


def write(args):
    pwm = PCA9685(args.address)
    pwm.set_pwm_freq(args.freq)
    pwm.write(args.addr, args.value)
    time.sleep(0.02)


def server(args):
    print(f'Starting websocket server at 0.0.0.0:{args.port}')
    if args.mock:
        ServerDelegate.RobotClass = MockRobot
        ServerDelegate.TankTurretClass = MockTankTurret

    try:
        server = SimpleWebSocketServer('0.0.0.0', args.port, ServerDelegate)
        server.serveforever()
    except KeyboardInterrupt:
        print('Stopping server')
        exit(0)


parser = ArgumentParser()
# parser.add_argument('-c', '--config', type=str, default='config.toml')
parser.add_argument('-a', '--address', type=int, default=0x40)
parser.add_argument('-c', '--channel', type=int, default=1)
parser.add_argument('-f', '--freq', type=int, default=50)
parser.add_argument('--debug', action='store_const', const=True, default=False)

subparsers = parser.add_subparsers()

pulse_parser = subparsers.add_parser('pulse')
pulse_parser.add_argument('value', type=int, default=0)
pulse_parser.set_defaults(handler=pulse)

write_parser = subparsers.add_parser('write')
write_parser.add_argument('addr', type=int, default=0)
write_parser.add_argument('value', type=int, default=0)
write_parser.set_defaults(handler=write)

server_parser = subparsers.add_parser('server')
server_parser.add_argument('-m', '--mock', action='store_const', const=True, default=False)
server_parser.add_argument('--port', type=int, default=8000)
server_parser.set_defaults(handler=server)

args = parser.parse_args()

logging.basicConfig(
    format='%(asctime)-15s %(levelname)-6s %(message)s',
    level=logging.DEBUG if args.debug else logging.INFO
)

if hasattr(args, 'handler'):
    args.handler(args)
else:
    parser.print_help()

