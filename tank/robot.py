import logging

import serial
import time


class Robot:
    def __init__(self):
        self.logger = logging.getLogger('Robot')

    def toHexFromByte(self, val):
        # return hex(ord(val))[2:].rjust(2,'0').upper()
        hex = val.replace('\n', '')
        hex = ''.join(["%02X" % ord(x) for x in hex]).strip()
        hex = hex.replace('\n', '')
        return hex

    def battery(self):  # --not tested
        # returns % of charge left and updates battery
        # self.ser.write([132]) #go to safe mode
        serial.flushInput()
        self.ser.flushOutput()  # added
        Data = [142, 25]
        # Data = [148,1,25]
        self.ser.write(Data)  # ask for info about battery charge
        time.sleep(1)  # added - sleep and timeout for test
        """Data = [148,1,25]
        self.ser.write(Data) #ask for info about battery charge - start streaming
        found = False
        counter = 0
        val = []
        idx1 = 0 #idx of data byte 1
        idx2 = 0 #dx of data byte 
        val.append(self.ser.read(30)) #read 2 bytes - uint16
        while not(found) or counter > 30:
            #for i in range(30):
            i = counter

            if int(self.toHexFromByte(val[i]),16) == 25:
                idx1 = i+1
                idx2 = i+2
            elif i == idx2:
                found = True
                self.ser.write([150,0]) #stop stream
                break
            counter = counter+1
        print("counter = ",counter)
        byte1 = self.toHexFromByte(val[idx1])
        print "hex_battery1 = ", byte1
        byte2 = self.toHexFromByte(val[idx2])
        print "hex_battery2 = ", byte2"""

        val = self.ser.read(2)  # read 2 bytes - uint16
        self.logger.debug("koniec wczytywania, dane = %s", val)
        hex = self.toHexFromByte(val)
        self.logger.debug("hex_battery = %x", val)  # 32767 #todo: otrzymujemy mah, wiec dzielenie powinno byc przez max pojemnosc w mah?

        percentage = (int(hex, 16) * 100) / 3200
        self.logger.debug("battery %% = %s", percentage)
        del Data[:]

        # self.ser.write([142,25]) #ask for info about battery capacity?
        # val = self.ser.read(2)
        # hex = self.toHexFromByte(val)
        # cap = (int(hex,16)) #32767
        # print "baterry maxcap= ", cap

        return percentage

    def prepare(self):
        # opening port, sending starting commands
        self.logger.debug("preparing robot")
        self.ser = serial.Serial(
            port='/dev/ttyUSB0',
            baudrate=57600,
            parity=serial.PARITY_ODD,
            stopbits=serial.STOPBITS_ONE,
            bytesize=serial.EIGHTBITS,
            timeout=2
        )
        self.ser.isOpen()

        FullMode = [128, 132]
        Song = [140, 1, 7, 76, 13, 76, 13, 76, 13, 72, 13, 76, 13, 79, 30, 67, 10, 141, 1]
        LED = [139, 10, 200, 255]
        self.ser.write(FullMode)
        # self.ser.write(Song)
        self.ser.write(LED)
        # time.sleep(2) #wait 2s for Song and LED
        self.logger.debug("robot online")

    def extractKBits(self, num, k, p):
        # Function to extract k bits from a given
        # position in a number

        # convert number into binary first
        binary = '{:016b}'.format(num)

        end = len(binary) - p
        start = end - k + 1
        # extract k bit sub-string
        kBitSubStr = binary[start: end + 1]
        # convert extracted sub-string into decimal again
        result = int(kBitSubStr, 2)
        return result

    def drive(self, speed, angle):
        # speed<-10;10>, angle<-90;90>
        # negative speed = move back;  negative angle move left

        if (not (speed >= -10 and speed <= 10)):
            # error
            self.stop()
            self.logger.warning("error: wrong speed")
            return
        if (not (angle >= -90 and speed <= 90)):
            # error
            self.stop()
            self.logger.warning("error: wrong angle")
            return

        # numbers are passed in u2 16b format
        # values closer to center(32768) => higher speed
        # angle is more like radius, center(32768 or 32767) is driving straight
        center_uint16 = 32767
        maxNegative = 65535
        stepSpeed = int(500 / 10)  # (-500,500)[mm/s]
        stepRadius = int(2000 / 90)  # (-2000,2000)[mm]
        # stepRadius = int(32767 / 90)

        if speed >= 0:
            velocity = int(stepSpeed * speed)
        else:
            velocity = int(maxNegative + stepSpeed * speed)
        velHighByte = self.extractKBits(velocity, 8, 9)
        velLowByte = self.extractKBits(velocity, 8, 1)

        # radius = int(32768 + stepRadius*angle) 	#radius = 32768lub32767 <=>
        if angle > 0:
            # radius = int(stepRadius*angle)
            radius = int((65535 - 2000) + stepRadius * angle)
        elif angle == 0:
            radius = 32767
        elif angle < 0:
            # radius = int(65535+stepRadius*angle)
            radius = int(2000 + stepRadius * angle)
        radHighByte = self.extractKBits(radius, 8, 9)
        radLowByte = self.extractKBits(radius, 8, 1)
        Data = [137, velHighByte, velLowByte, radHighByte, radLowByte]

        self.ser.write(Data)
        del Data[:]

    def stop(self):
        # stops moving
        Data = [137, 0, 0, 0, 0]  # stop
        self.ser.write(Data)
        del Data[:]

    def turn(self, direction):
        # stops and starts rotating
        self.stop()

        if direction == "left":
            Data = [137, 1, 0, 0, 0]
        elif direction == "right":
            Data = [137, 1, 0, 255, 255]

        self.ser.write(Data)
        del Data[:]

    def pause(self):
        # ends communication with robot, and roomba will switch to powersaving
        # Safe = 131 #safe mode
        StopOI = 173  # alternative: [128] -passive mode
        self.ser.write([StopOI])
        self.logger.debug("robot offline")


class MockRobot:
    def __init__(self):
        self.logger = logging.getLogger('mock-robot')

    def prepare(self):
        self.logger.debug('Would connect to iRobot')

    def drive(self, drive, turn):
        self.logger.debug('Drive(%s, %s)', turn, drive)

    def stop(self):
        self.logger.debug('Stop moving')

    def pause(self):
        self.logger.debug('Disconnecting from iRobot')
