import json
import logging
from SimpleWebSocketServer import WebSocket

from tank.robot import Robot
from tank.turret import TankTurret


def _clamp(val, low, high):
    return min(max(val, low), high)


class ServerDelegate(WebSocket):
    RobotClass = Robot
    TankTurretClass = TankTurret

    def __init__(self, server, sock, address):
        super().__init__(server, sock, address)
        self.logger = logging.getLogger('ServerDelegate')
        self.actions = {
            'move': self.do_move
        }
        self.turret = ServerDelegate.TankTurretClass(axes=[1, 2])
        self.robot = ServerDelegate.RobotClass()
        self.robot.prepare()  # get ready, play song and LED

    def handleConnected(self):
        addr, port = self.address
        self.logger.info(f'Client connected: {addr}:{port}')

    def handleMessage(self):
        data = self.data
        try:
            data = json.loads(data)
        except json.JSONDecodeError:
            self._send_error('Invalid JSON frame')
            return

        self.logger.debug('Received frame %s', data)

        action_name = data.get('action')
        if action_name in self.actions:
            self.actions[action_name](data)
        else:
            self._send_error('Unknown action')

    def handleClose(self):
        addr, port = self.address
        self.logger.info(f'Client connected: {addr}:{port}')
        self.turret.close()
        self.robot.pause()

    def do_move(self, frame):
        data = frame.get('data')
        base_x, base_y = data.get('base_x', 0.0), data.get('base_y', 0.0)
        arm_x, arm_y = data.get('arm_x', 0.0), data.get('arm_y', 0.0)

        if not any([isinstance(base_x, float), isinstance(base_y, float), isinstance(arm_x, float), isinstance(arm_y, float)]):
            self._send_error('Invalid frame parameters')
            return

        base_x = _clamp(base_x, -1, 1)
        base_y = _clamp(base_y, -1, 1)
        arm_x = _clamp(arm_x, -1, 1)
        arm_y = _clamp(arm_y, -1, 1)

        if abs(base_x) < 0.05 and abs(base_y) < 0.05:
            self.robot.stop()
        else:
            self.robot.drive(base_y * 10, base_x * 90)
        self.turret.rotate(0, arm_x)
        self.turret.rotate(1, arm_y)

    def _send_json(self, data):
        frame = {'ok': True, 'data': data}
        self.logger.debug('Sending response frame %s', frame)
        self.sendMessage(json.dumps(frame))

    def _send_error(self, message):
        self.logger.debug('Sending error frame: %s', message)
        frame = {'ok': False, 'error': message}
        self.sendMessage(json.dumps(frame))
