from abc import ABC, abstractmethod
import threading
import time
import logging
from queue import Queue


class TankAxis:
    def __init__(self, channel=1, speed=5, base=1600):
        self.pwm = None
        self.channel = channel

        self.speed = speed
        self.base = base
        self.position = base

    def apply(self, delta):
        self.position += delta * self.speed

    def update(self):
        self.pwm.set_servo_pulse(self.channel, self.position)

    def stop(self):
        self.position = self.base


class TankTurretABC(ABC):
    @abstractmethod
    def rotate(self, axis, direction):
        ...

    @abstractmethod
    def stop(self, axis):
        ...

    @abstractmethod
    def close(self):
        ...


class TankTurret(TankTurretABC):
    def __init__(self, axes, hat_address=0x40, ticks=5):
        from tank.util.pca9685 import PCA9685
        self.pwm = PCA9685(address=hat_address)
        self.thread = threading.Thread(target=self._thread_main)
        self.ticks = ticks
        self.axes = axes
        self.deltas = [{'delta': 0, 'ticks': 0} for _ in self.axes]

        for axis in self.axes:
            axis.pwm = self.pwm

        # Queue for communicating with pcm thread
        self._task_queue = Queue()

        # For killing pcm thread
        self._task_end = threading.Event()
        self.thread.start()

    def rotate(self, axis, direction):
        self._task_queue.put((axis, direction))

    def stop(self, axis):
        self._task_queue.put((axis, 0))

    def close(self):
        self._task_end.set()

    def _thread_main(self):
        logger = logging.getLogger('i2c-hat-thread')
        self.pwm.set_pwm_freq(50)

        for axis in self.axes:
            axis.update()

        while not self._task_end.is_set():
            if not self._task_queue.empty():
                message = self._task_queue.get_nowait()
                logger.debug('Got message %s', message)
                self._handle_message(message)

            for axis, delta in enumerate(self.deltas):
                if delta['ticks'] > 0:
                    logger.debug('Processing axis %s with delta %s', axis, delta)
                    self.axes[axis].apply(self.deltas[axis]['delta'])
                    delta['ticks'] -= 1
                    logger.debug('Axis %s position: %s', axis, self.axes[axis].position)
                elif delta['ticks'] == 0:
                    # channel = self.axes[axis]
                    self.axes[axis].stop()
                    # self.pwm.set_servo_pulse(channel, 0)

            for axis in self.axes:
                axis.update()

            time.sleep(0.05)  # 20/second

        logger.info('Closing thread')
        for axis in self.axes:
            axis.stop()
            axis.update()

    def _handle_message(self, message):
        axis, delta = message
        if delta < -1:
            delta = -1
        elif delta > 1:
            delta = 1

        if not (0 <= axis < len(self.axes)):
            return

        self.deltas[axis] = {'delta': delta, 'ticks': self.ticks}


class MockTankTurret(TankTurretABC):
    def __init__(self, axes):
        self.logger = logging.getLogger('mock-turret')

    def rotate(self, axis, direction):
        self.logger.info('Rotating axis %s with %s', axis, direction)

    def stop(self, axis):
        self.logger.info('Stopping axis %s')

    def close(self):
        self.logger.info('Closing connection')

